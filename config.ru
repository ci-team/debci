require 'debci/app'
require 'debci/home'
require 'debci/api'
require 'debci/self_service'
require 'debci/frontend'
require 'debci/status'
require 'debci/admin'
require 'debci/feeds'
require 'localhost' if ENV['RACK_ENV'] == 'development'

LISTING = <<~HTMLBLOCK.freeze
  <h1>Index of <%= request.path %></h1>
  <div><a href="..">..</a></div>
  <% Dir.chdir(@dir) do %>
    <% Dir.glob('*').each do |f| %>
      <% h = File.directory?(f) ? f + '/': f %>
      <div><a href="<%= h %>"><%= f %></a></div>
    <% end %>
  <% end %>
HTMLBLOCK

if ENV['RACK_ENV'] == 'development'
  class ServeStatic < Debci::Home
    set :static, true
    set :public_folder, 'public'
    def static!(*args)
      # XXX static! is a private method, so this could break at some point
      path = File.expand_path("#{settings.public_folder}#{URI_INSTANCE.unescape(request.path_info)}")
      if File.file?(path) && path =~ /log\.gz$/
        headers['Content-Encoding'] = 'gzip'
        headers['Content-Type'] = 'text/plain; charset=utf-8'
      end
      if request.path_info != "/" && File.directory?(path)
        redirect("#{request.path}/") if request.path !~ %r{/$}
        index = File.join(path, "index.html")
        if File.exist?(index)
          send_file(index)
        else
          @dir = path
          halt(200, erb(LISTING))
        end
      end
      super
    end
  end
end

app = Rack::Builder.new do
  if ENV['RACK_ENV'] == 'development'
    run ServeStatic
  else
    run Debci::Home
  end
  map '/packages' do
    run Debci::Frontend
  end
  map '/api' do
    run Debci::API
  end
  map '/user' do
    run Debci::SelfService
  end
  map '/status' do
    run Debci::Status
  end
  map '/admin' do
    run Debci::Admin
  end
  map '/data/feeds' do
    run Debci::Feeds
  end
end

run app
