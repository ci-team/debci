require 'thor'
require 'debci/html'
require 'debci/package'

module Debci
  class HTML
    class CLI < Thor
      desc 'update', "Updates global HTML pages"
      def update
        Debci::HTML.update
      end

      def self.exit_on_failure?
        true
      end
    end
  end
end
