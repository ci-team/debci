require 'debci/app'
require 'debci/feed'
require 'debci/package'

module Debci
  class Feeds < Debci::App
    get '/:prefix/:package.xml' do
      pkg = Debci::Package.find_by_name(params[:package])
      halt 404 unless pkg
      halt 404 unless pkg.prefix == params[:prefix]

      headers['Content-Type'] = 'application/xml'
      Debci::Feed.new(pkg.news) do |feed|
        feed.channel.title = "#{pkg.name} CI news feed"
        feed.channel.about = Debci.config.url_base + "/packages/#{pkg.prefix}/#{pkg.name}/"
        feed.channel.description = [
          "News for #{pkg.name}.",
          'Includes only state transitions (pass-fail, and fail-pass).',
          'Full history is available in the package page and in the published data files.',
        ].join(' ')
      end.to_s
    end
  end
end
