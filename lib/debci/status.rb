require "debci/app"
require 'debci/html_helpers'
require 'debci/graph'

module Debci
  class Status < Debci::App
    include Debci::HTMLHelpers
    set :views, "#{File.dirname(__FILE__)}/html/templates"

    configure do
      set :suites, Debci.config.suite_list
      set :archs, Debci.config.arch_list
    end

    PER_PAGE = 200

    def filter_and_paginate(jobs)
      query = params.select { |k| [:arch, :suite].include?(k.to_sym) }
      get_page_params(jobs.where(query), params[:page], PER_PAGE)
    end

    before do
      redirect "#{request.path}/" if request.path !~ %r{/$}
    end

    get "/" do
      erb :status
    end

    get "/alerts/" do
      @tmpfail = filter_and_paginate(Debci::Job.tmpfail)
      @alert_number = @tmpfail.total_records

      erb :status_alerts
    end

    get "/failing/" do
      @jobs = filter_and_paginate(Debci::Job.fail)
      @failing_number = @jobs.total_records
      erb :status_failing
    end

    get "/pending/" do
      @pending = filter_and_paginate(Debci::Job.pending)
      erb :status_pending_jobs
    end

    get "/reject_list/" do
      @reject_list = Debci.reject_list
      erb :reject_list
    end

    get "/platform-specific-issues/" do
      @issues = Debci::Job.platform_specific_issues
      erb :platform_specific_issues
    end

    get "/slow/" do
      @slow = filter_and_paginate(Debci::Job.slow.includes(:package).order("packages.name"))
      erb :status_slow
    end
  end
end
