class IndexJobDate < Debci::DB::LEGACY_MIGRATION
  disable_ddl_transaction!

  def change
    opts = { algorithm: :concurrently } if ActiveRecord::Base.connection.index_algorithms.has_key?(:concurrently)

    opts ||= {}
    add_index :jobs, :date, **opts
  end
end
