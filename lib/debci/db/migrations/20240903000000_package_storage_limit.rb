class PackageStorageLimit < Debci::DB::LEGACY_MIGRATION
  def change
    add_column :packages, :storage_limit, :integer, null: true
  end
end
