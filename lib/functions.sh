#!/bin/sh

case $- in
  *i*)
    ;;
  *)
    set -eu
    ;;
esac

autopkgtest_dir_for_package() {
  local pkg="$1"
  pkg_dir=$(echo "$pkg" | sed -e 's/\(\(lib\)\?.\).*/\1\/&/')
  echo "${debci_autopkgtest_dir}/${pkg_dir}"
}

autopkgtest_incoming_dir_for_package() {
  local pkg="$1"
  pkg_dir=$(echo "$pkg" | sed -e 's/\(\(lib\)\?.\).*/\1\/&/')
  echo "${debci_autopkgtest_incoming_dir}/${pkg_dir}"
}

log() {
  if [ "$debci_quiet" = 'false' ]; then
    echo "$@"
  fi
}

log_error() {
  echo "$@"
}

maybe_with_proxy() {
  if command -v auto-apt-proxy >/dev/null; then
    export http_proxy=$(auto-apt-proxy 2>/dev/null || true)
  fi
  "$@"
}
