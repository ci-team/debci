require 'spec_helper'
require 'debci/package'
require 'debci/html/cli'

describe Debci::HTML::CLI do
  it 'calls update on update' do
    expect(Debci::HTML).to receive(:update)
    Debci::HTML::CLI.new.update
  end
end
