require 'spec_helper'
require 'debci/package'
require 'debci/backend'

describe Debci::Backend do
  let(:package) { Debci::Package.new }
  let(:qemu_package) { Debci::Package.new(backend: "qemu") }
  let(:default_backend) { Debci.config.backend }

  before(:each) do
    expect(Debci.config).to receive(:backend_list).and_return(["qemu", "lxc"])
    allow(Debci::AMQP).to receive(:queue_exists?).and_return(false)
  end

  it 'selects the default backend if package does not specify one' do
    expect(Debci::Backend.select(package, "arm64")).to eq(default_backend)
  end

  it 'selects default backend if requested is not available' do
    expect(Debci::Backend.select(qemu_package, "arm64")).to eq(default_backend)
  end

  it 'selects the requested backend if available' do
    expect(Debci::AMQP).to receive(:queue_exists?).with("arm64", "qemu").and_return(true)
    expect(Debci::Backend.select(qemu_package, "arm64")).to eq("qemu")
  end

  it 'selects next backend in the list if first is not available' do
    expect(Debci::AMQP).to receive(:queue_exists?).with("arm64", "lxc").and_return(true)
    expect(Debci::Backend.select(qemu_package, "arm64")).to eq("lxc")
  end
end
