require "spec_helper"
require 'spec_mock_server'
require 'rack/test'
require 'debci/home'

describe Debci::Home do
  include Rack::Test::Methods

  class Home < Debci::Home
    set :raise_errors, true
    set :show_exceptions, false
  end

  def app
    mock_server('/', Home)
  end

  it 'renders the home page with no data' do
    get '/'
    expect(last_response.status).to eq(200)
    expect(last_response.body).to match("(no data yet)")
  end

  it 'renders the home page with packages' do
    Debci::Package.create!(name: "foobar")
    get '/'
    expect(last_response.status).to eq(200)
    expect(last_response.body).to_not match("(no data yet)")
  end
end
