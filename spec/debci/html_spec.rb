require 'json'
require 'pathname'
require 'spec_helper'
require 'debci/html'

describe Debci::HTML do
  let(:html) { Pathname(Debci.config.html_dir) }
  let(:data) { Pathname(Debci.config.data_basedir) }

  context 'generating global pages' do
    before(:each) { Debci::HTML.update }
  end

  let(:package) { Debci::Package.create!(name: 'foobar') }

  let(:theuser) { Debci::User.create!(username: 'user') }

  let(:theuser) { Debci::User.create!(username: 'debci') }

  context 'producing JSON data' do
    before do
      Debci::Job.create!(
        package: package,
        suite: 'unstable',
        arch: arch,
        requestor: theuser,
        status: 'pass',
        date: Time.now,
        duration_seconds: 42,
        version: '1.0-1',
      ).tap do |j|
        (data / ("autopkgtest/unstable/#{arch}/f/foobar/%<id>d" % { id: j.id })).mkpath
      end
      Debci::HTML.update
    end

    it 'produces status.json' do
      status = data / "status/unstable/#{arch}/status.json"
      expect(status).to exist
      status = ::JSON.parse(status.read)
      expect(status["pass"]).to eq(1)
      Time.parse(status["date"])
    end

    it 'produces status of the day' do
      today = data / Time.now.strftime("status/unstable/#{arch}/%Y/%m/%d.json")
      expect(today).to exist
      status = ::JSON.parse(today.read)
      expect(status["pass"]).to eq(1)
    end

    it 'produces history.json' do
      history = data / "status/unstable/#{arch}/history.json"
      expect(history).to exist
      history = ::JSON.parse(history.read)
      expect(history.first["pass"]).to eq(1)
    end

    it 'produces packages.json' do
      packages = data / "status/unstable/#{arch}/packages.json"
      expect(packages).to exist
      packages = ::JSON.parse(packages.read)
      expect(packages.first["package"]).to eq("foobar")
      expect(packages.first["status"]).to eq("pass")
    end
  end
end
