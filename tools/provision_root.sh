#!/bin/bash

set -eux
source /etc/os-release

export DEBIAN_FRONTEND=noninteractive
echo "deb http://deb.debian.org/debian ${VERSION_CODENAME}-backports main" > /etc/apt/sources.list.d/backports.list
apt-get -y update
apt-get -qqyt ${VERSION_CODENAME}-backports install autopkgtest
apt-get -qqy install make ruby git debootstrap

base=$(dirname $0)/..
cd ${base}
apt-get -qqy build-dep .
apt-get -qqy install ruby-simplecov

# provision as non-root for development
runuser -u "$(stat --format=%U .)" tools/provision_user.sh
