#!/bin/bash

set -eux

cd $(dirname $0)/..
./tools/init-dev.sh
make
./bin/debci migrate
set +x
echo "Development virtual machine is installed!"
